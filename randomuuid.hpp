#ifndef RANDOMUUID_HPP
#define RANDOMUUID_HPP

#include <random>
#include <sstream>

using namespace std;

//these random functions from here:
// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3551.pdf
std::default_random_engine & global_urng( )
{
	static std::default_random_engine u {};
	return u;
}
void randomize( )
{
	static std::random_device rd {};
	global_urng().seed( rd() );
}
int randomInRange( int from, int thru )
{
	static std::uniform_int_distribution<> d {};
	using parm_t = decltype(d)::param_type;
	return d( global_urng(), parm_t {from, thru} );
}
double randomInRange( double from, double upto )
{
	static std::uniform_real_distribution<> d {};
	using parm_t = decltype(d)::param_type;
	return d( global_urng(), parm_t {from, upto} );
}

//generate a single version 4 (random) uuid
string getUuid()
{
	stringstream uuid_str;
	int four_low = 4096;
	int four_high = 65535;
	int three_low = 256;
	int three_high = 4095;
	uuid_str << std::hex << randomInRange(four_low,four_high);
	uuid_str << std::hex << randomInRange(four_low,four_high);
	uuid_str << "-" << std::hex << randomInRange(four_low,four_high);
	uuid_str << "-" << std::hex << randomInRange(four_low,four_high);
	uuid_str << "-4" << std::hex << randomInRange(three_low,three_high);
	uuid_str << "-8" << std::hex << randomInRange(three_low,three_high);
	uuid_str << std::hex << randomInRange(four_low,four_high);
	uuid_str << std::hex << randomInRange(four_low,four_high);
	return uuid_str.str();
}

#endif // RANDOMUUID_HPP
