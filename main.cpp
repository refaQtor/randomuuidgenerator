#include <iostream>
#include <string>
#include <cstring>

#include "randomuuid.hpp"

using namespace std;

const string VERSION_TEXT = "0.1.1";
const string HELP_TEXT =
    "\n\'randomuuid' - plain and simple, \ngenerator of random numbers and version 4 random uuid."
    "\n\nUSAGE: randomuuid [option] <value>\n\nOPTIONS:"
    "\n--integer \t-i <count> \tproduce <count> random numbers between 0 and 1000"
    "\n--real \t\t-r <count> \tproduce <count> random numbers between 0.0 and 1.0"
    "\n--uuid \t\t-u <count>  \tproduce <count> random uuids"
    "\n\nRandom functions base on information in this paper:"
    "\nhttp://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3551.pdf"
    "\nsource code at: bitbucket.org/refaQtor/randomuuidgenerator\n";

int main(int argc, char **argv)
{
    if (argc > 1) {        
        randomize(); //prep the engine
        int count = 1;
        if (argc > 2)
              count = stoi(argv[2]);
        if ((strcmp(argv[1], "-r") == 0) || (strcmp(argv[1],"--real") == 0)) {
			double min_double = 0.0;
			double max_double = 1.0;
			do {
				cout << randomInRange(min_double, max_double) << endl;
				count--;
			} while (count > 0);
        } else if ((strcmp(argv[1], "-i") == 0) || (strcmp(argv[1],"--integer") == 0)) {
			int min_integer = 0;
			int max_integer = 1000;
			do {
				cout << randomInRange(min_integer, max_integer) << endl;
				count--;
			} while (count > 0);
        } else if ((strcmp(argv[1], "-u") == 0) || (strcmp(argv[1],"--uuid") == 0)) {
			do {
				cout << getUuid() << endl;
				count--;
			} while (count > 0);
        } else { //we don't understand, you need to see help text
			cout << HELP_TEXT << "\nVERSION: " << VERSION_TEXT << endl;
		}
	} else { //we don't understand, you need to see help text
        cout << HELP_TEXT << "\nVERSION: " << VERSION_TEXT << endl;
	}
    return 0;
}
