```
'randomuuid' - plain and simple, 
generator of random numbers and version 4 random uuid.

USAGE: randomuuid [option] <value>

OPTIONS:
--integer       -i <count>      produce <count> random numbers between 0 and 1000
--real          -r <count>      produce <count> random numbers between 0.0 and 1.0
--uuid          -u <count>      produce <count> random uuids

Random functions base on information in this paper:
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3551.pdf

source code at: bitbucket.org/refaQtor/randomuuidgenerator

VERSION: 0.1.1
```

To build the simple application:

```
mkdir build
cd build
cmake ..
make
```

To include the generator into your own C++11 application, simply:

```
#include "randomuuid.hpp"
```

It works for me. Do as you please.
